---
title: "Bienvenu sur ce site de présentation de cartes yu-gi-oh"
---

Le but de ce site est de montrer des cartes **yu-gi-oh** et afficher les détails de la carte

Les détails d'une carte sont :

- **La description** qui correspond à l'effet de la carte.
- **L'archétype** qui correspond a la famille du monstre.
- **Les séries** qui correspondent aux packs dans lesquels cette carte a été présente.
