import Link from "next/link";
import axios from "axios";
import styles from "../index.module.css";
import { motion } from "framer-motion";

export async function getStaticProps() {
  const res = await axios.get('https://db.ygoprodeck.com/api/v7/cardinfo.php?banlist=tcg&num=60&offset=70');
  const cards = res.data;

  return {
    props: {
      cards: cards.data
    }
  };
}

export default function Cards({ cards }) {
  let banned = [];
  let limited = [];
  let semi_limited = [];
  cards.map((card, i) => {
    if (card['banlist_info']["ban_tcg"] == "Banned") {
      banned.push(card);
    }
    else if (card['banlist_info']["ban_tcg"] == "Limited") {
      limited.push(card);
    }
    else if (card['banlist_info']["ban_tcg"] == "Semi-Limited") {
      semi_limited.push(card);
    }
  });

  return (
    <div className={styles.root_card}>
      <div className={styles.cards_header}>
        <h1>banlist yu-gi-oh</h1>
        <h2>Voici une liste non exhaustive des cartes présentes dans la banlist yu gi oh triée par limite d'exemplaire dans le deck</h2>
      </div>
      <div className={styles.cards_array}>
        <div>
          <h3 className={styles.h3_banned}>Cartes bannies</h3>
          <div className={styles.cards}>
            {banned.map(({ id, card_images }) => (
              <motion.div
                transition={{ duration: 0.1 }}
                whileHover={{ scale: 1.1 }}
                key={id}
                className={styles.card}
              >
                <Link href={`/cards/${id}`}>
                  <a>
                    <img src={card_images[0]['image_url']} alt="" />
                  </a>
                </Link>
              </motion.div>
            ))}
          </div>
        </div>
        <div>
          <h3 className={styles.h3_limited}>Cartes limitées</h3>
          <div className={styles.cards}>
            {limited.map(({ id, card_images }) => (
              <motion.div
                transition={{ duration: 0.1 }}
                whileHover={{ scale: 1.1 }}
                key={id}
                className={styles.card}
              >
                <Link href={`/cards/${id}`}>
                  <a>
                    <img src={card_images[0]['image_url']} alt="" />
                  </a>
                </Link>
              </motion.div>
            ))}
          </div>
        </div>
        <div>
          <h3 className={styles.h3_semi_limited}>Cartes semi-limitées</h3>
          <div className={styles.cards}>
            {semi_limited.map(({ id, card_images }) => (
              <motion.div
                transition={{ duration: 0.1 }}
                whileHover={{ scale: 1.1 }}
                key={id}
                className={styles.card}
              >
                <Link href={`/cards/${id}`}>
                  <a>
                    <img src={card_images[0]['image_url']} alt="" />
                  </a>
                </Link>
              </motion.div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}
