import axios from "axios";
import Link from "next/link";
import styles from "../../index.module.css";
import { motion } from "framer-motion";

export async function getStaticPaths() {
  const res = await axios.get('https://db.ygoprodeck.com/api/v7/cardinfo.php/');
  const cards = res.data;

  return {
    paths: cards.data.map(({ id }) => `/cards/${id}`),
    fallback: false
  };
}

export async function getStaticProps({ params }) {
  const { id } = params;

  const res = await axios.get(
    `https://db.ygoprodeck.com/api/v7/cardinfo.php/?id=${id}&language=fr`
  );
  const card = res.data;

  if (card.data[0]['archetype']) {
    const res2 = await axios.get(
      `https://db.ygoprodeck.com/api/v7/cardinfo.php?archetype=${card.data[0]['archetype']}&num=10&offset=0`
    );
    const archetype = res2.data;
    return {
      props: {
        card: card.data,
        archetype: archetype.data,
      }
    };
  }
  else {

    return {
      props: {
        card: card.data,
        archetype: 0,
      }
    };
  }


  return {
    props: {
      card: card.data,
      archetype: "",
    }
  };
}

export default function Card({ card, archetype }) {
  console.log(card);
  if (archetype) {
    return (
      <div className={styles.root_card}>
        <h1>Détails de la carte</h1>

        <div className={styles.card_details}>
          <img src={card[0]['card_images'][0]['image_url']} alt="" />
          <div className={styles.card_info}>
            <div>
              <h2>{card[0]['name']}</h2>
            </div>
            <div className={styles.card_info_effect}>
              <p>Type de cartes : {card[0]['type']}</p>
              <p>Attribut / propriété: {card[0]['type']}</p>
              <p>Effet : {card[0]['desc']}</p>
              <p>Elle est apparus dans les sets :</p>
              <ul>
                {card[0]['card_sets'].map(({ set_name, set_rarity }) => (
                  <li>{set_name} en rareté : {set_rarity}</li>
                ))}
              </ul>
            </div>
          </div>
        </div>
        <p>Exemples de cartes de l'archétype {card[0]['archetype']} : </p>
        <div className={styles.cards}>
          {archetype.map(({ id, card_images }) => (
            <motion.div
              transition={{ duration: 0.1 }}
              whileHover={{ scale: 1.1 }}
              key={id}
              className={styles.card}
            >
              <Link href={`/cards/${id}`}>
                <a>
                  <img src={card_images[0]['image_url']} alt="" />
                </a>
              </Link>
            </motion.div>
          ))}
        </div>
    </div>
    );
  }
  else {
    return (
      <div className={styles.root_card}>
        <h1>Détails de la carte</h1>

        <div className={styles.card_details}>
          <img src={card[0]['card_images'][0]['image_url']} alt="" />
          <div className={styles.card_info}>
            <div>
              <h2>{card[0]['name']}</h2>
            </div>
            <div className={styles.card_info_effect}>
              <p>Type de cartes : {card[0]['type']}</p>
              <p>Attribut / propriété: {card[0]['type']}</p>
              <p>Effet : {card[0]['desc']}</p>
              <p>Elle est apparus dans les sets :</p>
              <ul>
                {card[0]['card_sets'].map(({ set_name, set_rarity }) => (
                  <li>{set_name} en rareté : {set_rarity}</li>
                ))}
              </ul>
            </div>
          </div>
        </div>
        <div className={styles.cards}>
          <p>Cette carte ne fait partie d'aucun archétype</p>
        </div>
    </div>
    );
  }
}
