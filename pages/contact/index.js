import Link from "next/link";
import ContactForm from "./form";
import styles from "../index.module.css";

export default function Contact({ ygo }) {
  return (
    <div className={styles.root}>
      <h3>Contact Us</h3>
      <ContactForm />
    </div>
  );
}
