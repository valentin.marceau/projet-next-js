import React from 'react';
import styles from "../index.module.css";
import { useForm, ValidationError } from '@formspree/react';
function ContactForm() {
  const [state, handleSubmit] = useForm("mayvkdrk");
  if (state.succeeded) {
      return <p>Thanks for joining!</p>;
  }
  return (
      <form className={styles.form} onSubmit={handleSubmit}>
      <div className={styles.form_div}>
        <label htmlFor="email">
          Votre adresse mail
        </label>
        <input
          id="email"
          type="email"
          name="email"
          />
        <ValidationError
          prefix="Email"
          field="email"
          errors={state.errors}
          />
      </div>
      <label htmlFor="message">
        Votre message
      </label>
      <textarea
        id="message"
        name="message"
      />
      <ValidationError
        prefix="Message"
        field="message"
        errors={state.errors}
      />
      <button type="submit" disabled={state.submitting}>
        Envoyer
      </button>
    </form>
  );
}
function App() {
  return (
    <ContactForm />
  );
}
export default App;
