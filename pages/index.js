import Link from "next/link";
import axios from "axios";
import styles from "./index.module.css";

import { getPostData } from "../lib/markdown";

export default function Home({ postData }) {
  return (
      <div>
        <h1 className={styles.headingXl}>{postData.title}</h1>
        <div className={styles.lightText}>
        </div>
        <div dangerouslySetInnerHTML={{ __html: postData.contentHtml }} />
      </div>
  );
}

export async function getStaticProps({ params }) {
  const postData = await getPostData("home");

  return {
    props: {
      postData
    }
  };
}
