import Head from 'next/head'
import AppHeader from './appheader'
import AppFooter from './appfooter'
import styles from './layout.module.css'

export default function Layout({ children }) {
  return (
    <>
      <Head>
        <title>Layouts Example</title>
      </Head>
      <AppHeader></AppHeader>
      <main className={styles.main}>
      {children}
      </main>
      <AppFooter></AppFooter>
    </>
  )
}
