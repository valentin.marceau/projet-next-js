import Link from 'next/link'
import styles from './sidebar.module.css'

export default function AppHeader() {
  return (
    <nav className={styles.nav}>
      <div>
        <Link href="/">
          <a>Accueil</a>
        </Link>
      </div>
      <div>
        <Link href="/cards">
          <a>Les cartes</a>
        </Link>
        <Link href="/contact">
          <a>Nous contacter</a>
        </Link>
        <Link href="/licence">
          <a>Mentions légales</a>
        </Link>
      </div>
    </nav>
  )
}
