import path from "path";
import matter from "gray-matter";
import { remark } from "remark";
import html from "remark-html";
import fs from 'fs';

const postsDirectory = path.join(process.cwd(), "/pages/markdown");

export async function getPostData(name) {
  const fullPath = path.join(postsDirectory, `${name}.md`);
  const fileContents = fs.readFileSync(fullPath, "utf8");

  // Use gray-matter to parse the post metadata section
  const matterResult = matter(fileContents);

  // Use remark to convert markdown into HTML string
  const processedContent = await remark()
    .use(html)
    .process(matterResult.content);
  const contentHtml = processedContent.toString();

  // Combine the data with the id and contentHtml
  return {
    contentHtml,
    ...matterResult.data
  };
}
